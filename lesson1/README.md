# Lesson 1


## Information representation

Contents

*   [Computer](http://ecomputernotes.com/images/Computer.jpg)
*   [Binary](https://c65d6312-a-62cb3a1a-s-sites.googlegroups.com/site/syhsmata/creative-projects/binary-numbers/Binary.png?attachauth=ANoY7cqMO2tSGyxQ9M9okD1ocDHCHRW7tqViEIbr-PiKCKfrRW1qLOrSSZNxWguMhhhtkTJQQo4Rt5PFHBZo6-NQju08mqDaH7YXIUvxlJNUTfpVT-6LWlyucCT34lReoGZAcrqWvKPzzJhIVMaVHKiNh-8MrF6QRuvln56kO8tkyAFfnJV_TC69A2f6Jn7kHxFgrlrMs23iOXum-rEIq2AHCR5D-htFaVrOVlOf8rXuxXPfmpjJ2_yprRyQyVA4WZlJXkkvwY2p&attredirects=0)
*   [ASCII](https://ascii.cl/)
*   [RGB](https://www.w3schools.com/colors/colors_rgb.asp)


## Computer Program

Contents

*   Machine Code
*   Assembly Code
*   High Level Code


## Development Tool

Contents

*   Command Line Tool
    *   [Download Git](https://git-scm.com/downloads)
    *   [Reading Materials: Git Basics](https://git-scm.com/book/en/v2/Getting-Started-Git-Basics)
    *   [Online Course: Learn the Command Line](https://www.codecademy.com/learn/learn-the-command-line)
*   Source Code Management: Git
    *   [Online Course: Learn Git](https://www.codecademy.com/learn/learn-git)
*   Editor: VS Code
    *   [VS Code](https://code.visualstudio.com/docs)


Used Command Line in Today's Course

```sh
# list contents
$ ls

# entry directory
$ cd

# show current directory
$ pwd

# make directory
$ mkdir

# delete file
$ rm
```


Used Git Command in Today's Course

```sh
# config git author per directory
$ git config user.name "Your Name"
$ git config user.email "Your email"

# config git author globally
$ git config --global user.name "Your Name"
$ git config --global user.email "Your email"

# show current status
$ git status

# add file to staging area so git can track your file
$ git add <file name>

# commit your file to git
$ git commit -m "Message you want to write"

# show git commit history
$ git log
```
