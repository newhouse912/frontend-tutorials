# Frontend Tutorials

学习前端开发资料。


## Contents

[Lesson 1: 计算机基础](./lesson1/README.md)

*   信息的表示
*   计算机程序
*   开发工具
    *   命令行工具: Liunx commands
    *   代码版本管理工具: Git
    *   代码编辑器: VS Code


[Lesson 2: 前端基础](./lesson2/README.md)

*   市场上要求的前端技术
*   结构化标记语言: HTML
*   样式表单: CSS


[Lesson 3: 做一个 breakout 小游戏](./lesson3/README.md)

*   Node: JavaScript 运行时 (runtime)
*   Node package manager: npm
*   前端打包工具: webpack
*   初始化项目
*   HTML5: canvas 元素
    *   绘制画板
    *   绘制正方形
    *   绘制圆形
*   时间相应机制
*   动画
