# Lesson 2

*   市场上要求的前端技术
*   结构化标记语言: HTML
*   样式表单: CSS
*   前端编程语言: JavaScript
*   浏览器: Chrome, Firefox, IE


## 市场上要求的前端技术

国内外职位描述

*   [Indeed: User Experience (UX) Designer](https://www.indeed.jobs/career/JobDetail/User-Experience-UX-Designer/5858)
*   [Fast Retailing: UX/UI Designer](https://jp.indeed.com/%E4%BB%95%E4%BA%8B?jk=e32148f0d2ac1d55&tk=1clo0pgv20tel0th&from=serp&alid=3&advn=1351049121765701)
*   [Google: UX Engineer, Front End](https://careers.google.com/jobs#!t=jo&jid=/google/ux-engineer-front-end-345-spear-st-san-francisco-ca-94105-usa-3761940359&)
*   [Tencent CN: 交互设计师](https://hr.tencent.com/position_detail.php?id=43165&keywords=&tid=81&lid=0)
*   [Tencent CN: web前端开发工程师](https://hr.tencent.com/position_detail.php?id=43448&keywords=UI&tid=87&lid=0)
*   [XIAOMI CN: 高级UI设计师/UI设计师](http://job.hr.xiaomi.com/#/job/fdcb435e-5d16-49dc-ba24-61747d0ce4d9?_k=jgl64y)
*   [XIAOMI CN: 工业设计师](http://job.hr.xiaomi.com/#/job/4c209b7c-d8d9-4206-8cd2-af2b43a8ac3a?_k=bx6w39)
*   [XIAOMI CN: 前端开发工程师](http://job.hr.xiaomi.com/#/job/9f4d4670-ad7a-4347-879e-07392df15c33?_k=91m0ug)


需要什么技能

*   visual design
*   interaction design
*   HTML/CSS/JavaScript
*   Framework


## 结构化标记语言: HTML 和样式表单: CSS

基础讲解

*   [MDN Web Docs: Introduction to HTML](https://developer.mozilla.org/en-US/docs/Learn/HTML/Introduction_to_HTML)
*   [MDN Web Docs: How CSS works](https://developer.mozilla.org/en-US/docs/Learn/CSS/Introduction_to_CSS/How_CSS_works)


练习: 熟悉 HTML 标签和 CSS 样式

*   [HTML 中文教程](http://www.w3school.com.cn/html/index.asp)
*   [CSS 中文教程](http://www.w3school.com.cn/css/index.asp)


进阶练习

*   在线代码: [CodePen](https://codepen.io/)
*   设计分享: [Dribbble](https://dribbble.com/)


在 Dribbble 上面选择自己认为比较能够实现的设计用 HTML 和 CSS 实现成网页

*   [例子 1](https://dribbble.com/shots/2262761-Mobile-Blog-App-Interface/attachments/424147)
*   [例子 2](https://dribbble.com/shots/2314157-Daily-UI-Day-1/attachments/439137)
*   [例子 3](https://dribbble.com/shots/2639709-Confirm-Reservation/attachments/528798)
*   [例子 4](https://dribbble.com/shots/2144170-Day-014-Location-Card/attachments/392323)
*   [例子 5](https://dribbble.com/shots/2492038-Task-List-App/attachments/489171)


索引: 标签和样式

*   [HTML 标签](https://developer.mozilla.org/en-US/docs/Web/HTML/Reference)
*   [CSS 样式](https://developer.mozilla.org/en-US/docs/Web/CSS/Reference)


## 浏览器: Chrome, Firefox, IE

Google Chrome 浏览器自带开发工具

*   [Chrome inspector](https://developers.google.com/web/tools/chrome-devtools/?utm_source=dcc&utm_medium=redirect&utm_campaign=2018Q2)
