# Lesson 3: 做一个 breakout 小游戏

*   Node: JavaScript 运行时 (runtime)
*   Node package manager: npm
*   前端打包工具: webpack
*   初始化项目
*   HTML5: canvas 元素
    *   绘制画板
    *   绘制正方形
    *   绘制圆形
*   时间相应机制
*   动画


## Node: JavaScript 运行时 (runtime)

*   JavaScript 是如何在浏览器里面运行的
*   JavaScript 的运行时环境: [Nodejs](https://nodejs.org/en/)


小练习：写一个 nodejs 的 hello world 小程序

```javascript
$ node ./practice-nodejs/hello.js
```


## Node package manager: npm

*   软件的分发
*   前端软件的分发和管理: [npm](https://docs.npmjs.com/)
*   npm 是如何管理软件的: package.json


npm 命令

*   如何搜索软件: `npm search <package name>`
*   如何安装软件: `npm install [-g] <package name>`
*   如何查看已经安装的软件: `npm list`
*   如何卸载已经安装的软件: `npm uninstall`


## 前端打包工具: webpack

*   前端项目的文件组织: HTML/CSS/JS
*   大型项目的模块化
*   自动化文件管理和打包: webpack

小练习：写一个 webpack 的配置文件

*   [Webpack: get started](https://webpack.js.org/guides/getting-started/)


## 初始化项目

*   创建文件
*   设置好项目工具


## HTML5: canvas 元素

*   绘制画板
*   绘制正方形
*   绘制圆形


## 事件相应机制

*   事件的监听
*   事件的传播
*   事件的捕获: 回调函数


## 动画
